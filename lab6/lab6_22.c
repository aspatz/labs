#include "stdio.h"
#include "stdlib.h"


int main(void) {
    float firstArr[4], *ptr;
    
    for (int i = 0; i < 4; i++) {
        scanf("%f", &firstArr[i]);
    }

    printf("\n1)\n");
    ptr = &firstArr[0];
    for (int i = 0; i < 4; i++) {
        printf("%f\n", *(ptr+i));
    }
    
    ptr = (float *)calloc(4, sizeof(float));
    if (ptr == NULL) {
        printf("memory was not allocated\n");
        exit(1);
    }

    for (int i =0; i < 4; i++) {
        ptr[i] = firstArr[i];
    }

    printf("\n2)\n");
    for (int i = 0; i < 4; i++) {
        printf("%f\n", *(ptr+i));
    }
    free(ptr);
    return 0;
}
